<?php

namespace App\Http\Controllers\Front;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\View;

class ThanksController extends Controller
{

    /**
     * The vars to send.
     *
     * @var array
     */
    protected $locals = [];

    /**
     * Construc method
     *
     */
    public function __constructor()
    {
        //Some code
    }

    /**
     * Show home front page.
     *
     * @param  Symfony\Component\HttpFoundation\Request  $request
     * @return View
     */
    public function thanks(Request $request)
    {
        return view('thanks.thanks');
    }

}