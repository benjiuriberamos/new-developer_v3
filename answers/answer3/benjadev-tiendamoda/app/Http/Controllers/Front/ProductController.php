<?php
 
namespace App\Http\Controllers\Front;
 
use App\Traits\ProductFilterTrait;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Request;
use App\Models\{Category, Brand, Sector, Color, Size, Product};

class ProductController extends Controller
{
    use ProductFilterTrait;

    /**
     * The vars to send.
     *
     * @var array
     */
    protected $locals = [];

    /**
     * Construc method
     *
     */
    public function __constructor() {
        //Some code
    }

    /**
     * Show home front page.
     *
     * @param  int  $id
     * @return View
     */
    public function product(string $slug)
    {
        $detail = new Product();
        $detail = $detail->where('active', '1')
            ->where('slug', $slug)
            ->first()
            ;
        $this->locals['products_top'] =  Product::where('active', '=', '1')
            ->where('is_top', '=', '1')
            ->get()
            ;
        $this->locals['detail'] = $detail;
        return view('front.product', $this->locals);
    }

    /**
     * Show home front page.
     *
     * @param  Request  $request
     * @return View
     */
    public function products(Request $request)
    {
        $this->locals['precio_maximo'] =  Product::select(DB::raw('MAX(price + 0) as maxprice'))
                    ->get()
                    ->pluck('maxprice')
                    ->first();
        $this->locals['precio_minimo'] =  Product::select(DB::raw('MIN(price + 0) as minprice'))
                    ->get()
                    ->pluck('minprice')
                    ->first();
        
        $this->locals['products'] = $this->getFilterProducts($request);
        $this->locals['sectors'] = Sector::all();
        $this->locals['brands'] = Brand::all();
        $this->locals['categories'] = Category::all();
        $this->locals['sizes'] = Size::all();
        $this->locals['colors'] = Color::all();
        return view('front.products', $this->locals);
    }

}