<?php

namespace App\Http\Controllers\Front;

use App\Traits\ProductFilterTrait;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Models\{Category, Brand, Sector, Color, Size, Product};

class AjaxController extends Controller
{
    use ProductFilterTrait;

    /**
     * The vars to send.
     *
     * @var array
     */
    protected $locals = [];

    /**
     * Construc method
     *
     */
    public function __constructor()
    {
        //Some code
    }

    /**
     * Show home front page.
     *
     * @param  int  $id
     * @return View
     */
    public function products(Request $request)
    {
        $this->locals['products'] = $this->getFilterProducts($request);
        $this->locals['sectors'] = Sector::all();
        $this->locals['brands'] = Brand::all();
        $this->locals['categories'] = Category::all();
        $this->locals['sizes'] = Size::all();
        $this->locals['colors'] = Color::all();
        $response = [
            'success' => true,
            'html' => view('front.ajax.products', $this->locals)->render(),
        ];
        return new JsonResponse($response);
    }

}