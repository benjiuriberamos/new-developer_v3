<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use App\Models\PageAbout;
use App\Admin\Controllers\Subcore\SimplePageController;

class PageAboutController extends SimplePageController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Página Nosotros';

    /**
     * Routes to redirect .
     *
     * @var array
     */
    protected $routes = [
        'edit' => 'admin.about.edit',
        'update' => 'admin.about.update',
        //param name for crud see with comand "php artisan route:list"
        'param' => 'about',
    ];

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $model = PageAbout::firstOrCreate(array());
        $form = new Form($model);

        $form->tab('Títular', function ($form) {
            $form->switch('main_show', __('Bloque títular'))
                ->help('¿Mostrar bloque del títular?');
            $form->text('main_title', __('Títular'));
        });

        $form->tab('Historia', function ($form) {
            $form->switch('history_show', '')
                ->help('¿Mostrar bloque?');
            $form->text('history_title', __('Título'));
            $form->simpleeditor('history_description', __('Descripción'))
                ->simple();
            $form->image('history_image', '')
                ->removable();
            $form->switch('history_video_show', '')
                ->help('¿Mostrar vídeo?');
            $form->text('history_video_url', __('Enlace youtube'));
        });

        $form->tab('Misión y visión', function ($form) {
            $form->switch('about_show', __(''))
                ->help('¿Mostrar bloque de misión y visión?');
            $form->text('about_title', __('Título'));
            $form->collection('about_items', __('Items'), function ($table) {
                $table->text('title', __('Título'));
                $table->simpleeditor('text', __('Texto'))
                    ->full();
            });
        });

        $form->tab('Equipo', function ($form) {
            $form->switch('team_show', __(''))
                ->help('¿Mostrar bloque?');
            $form->text('team_title', __('Título'));
            $form->hasManyPro('members', 'Miembros', function ($table) {
                $table->switch('active', __(''))
                    ->help('¿Mostrar?');
                $table->text('name', __('Nombre'))
                    ->rules('required');
                $table->text('charge', __('Cargo'))
                    ->rules('required');
                $table->image('image', __('Imagen'))
                    ->removable()
                    ->rules('image:required');
                $table->url('facebook', __('Facebook'));
                $table->url('instagram', __('Instagram'));
                $table->url('twiter', __('Twiter'));
            });
        });
        
        $form->setAction(route($this->routes['update'], [$this->routes['param'] => 1]));

        return $form;
    }
}
