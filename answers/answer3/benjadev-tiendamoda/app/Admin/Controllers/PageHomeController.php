<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use App\Models\PageHome;
use App\Admin\Extensions\NestedForm;
use App\Admin\Controllers\Subcore\SimplePageController;

class PageHomeController extends SimplePageController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Página home';

    /**
     * Routes to redirect .
     *
     * @var array
     */
    protected $routes = [
        'edit' => 'admin.home.edit',
        'update' => 'admin.home.update',
        //param name for crud see with comand "php artisan route:list"
        'param' => 'home',
    ];

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $model = PageHome::firstOrCreate([]);
        $form = new Form($model);

        $form->tab('Banner', function ($form) {
            $form->switch('banner_show', '')
                ->help('¿Mostrar bloque?');
            $form->hasManyPro('banners', 'Banners', function ($form) {
                $form->image('imageb1', 'Imagen Desktop')
                    ->removable()
                    ->help('Seleccione el banner para desktop. Tamaño recomendado 1366x750');
                // $form->image('imageb2', 'Imagen Desktop')->removable();
                // $form->image('imageb3', 'Imagen Desktop')->removable();
                $form->text('title', 'Texto blanco')
                    ->help('Ingrese el texto blanco del banner.');
                $form->text('subtitle', 'Texto negro')
                    ->help('Ingrese el texto negro del banner.');
                $form->switch('btn_show', '¿Mostrar en botón?');
                $form->switch('btn_show', '¿Abrir en otra página?');
                $form->text('btn_text', 'Botón texto')
                    ->rules('max:35')
                    ->help('Ingrese el texto del botón.');
                $form->text('btn_url', 'Botón enlace')
                    ->rules('max:300')
                    ->help('Ingre la url del botón');

            })
            ->mode('table');
        });

        $form->tab('Nosotros', function ($form) {
            $form->switch('about_show', '')
                ->help('¿Mostrar bloque?');
            $form->text('about_title', 'Título')
                ->help('Ingrese el título del bloque.');
            $form->textarea('about_description', 'Descripción')
                ->help('Ingrese el descripción del bloque.');
            $form->image('about_image', 'Imagen')
                ->removable()
                ->help('Seleccione la imagen del bloque. Tamaño recomendado 1340x700.');
        });

        $form->tab('Sectores', function ($form) {
            $form->switch('sector_show', '')
                ->help('¿Mostrar bloque?');
        });
        $form->tab('Productos', function ($form) {
            $form->switch('product_show', '')
                ->help('¿Mostrar bloque?');
            $form->text('product_title', 'Título')
                ->help('Ingrese el título del bloque.');
        });
        $form->tab('Oferta', function ($form) {
            $form->switch('offert_show', '')
                ->help('¿Mostrar bloque?');
            $form->text('offert_title', 'Título')
                ->help('Ingrese el título del bloque.');
            $form->text('offert_subtitle', 'Subítulo')
                ->help('Ingrese el subtítulo del bloque.');
            $form->image('offert_image', 'Imagen Desktop')
                ->removable()
                ->help('Seleccione el banner para desktop. Tamaño recomendado 2340x840.');
            $form->switch('offert_btn_show', '¿Mostrar en botón?');
            $form->switch('offert_btn_show', '¿Abrir en otra página?');
            $form->text('offert_btn_text', 'Botón texto')
                ->rules('max:35')
                ->help('Ingrese el texto del botón.');
            $form->text('offert_btn_url', 'Botón enlace')
                ->rules('max:300')
                ->help('Ingre la url del botón');
        });
        $form->tab('Categorias', function ($form) {
            $form->switch('category_show', '')
                ->help('¿Mostrar bloque?');
            $form->text('category_title', 'Título')
                ->help('Ingrese el título del bloque.');
        });
        $form->tab('Marcas', function ($form) {
            $form->switch('clients_show', '')
                ->help('¿Mostrar bloque?');
            $form->multipleImage('clients_items', __('Clientes'))
                ->removable()
                ->help('Seleccione las imagenes. Tamaño recomendado 100x60.');
        });

        $form->setAction(route($this->routes['update'], [$this->routes['param'] => 1]));

        return $form;
    }
}
