<?php

namespace App\Admin\Controllers;

use App\Models\Brand;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Admin\Controllers\Subcore\CompletePageController;

class BrandController extends CompletePageController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Marcas de ropa';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Brand);

        $grid->column('id', __('ID'))->sortable();

        $grid->column('name', __('Nombre'));
        $grid->column('slug', __('Slug'));
        $grid->column('home_image', __('Imagen'))
            ->display(function ($name) {
                $url =  $name ? Storage::url($name) : false;
                $html =  $url ? "<img src=' $url' width='20' heigth='20'></img>" : 'Sin imagen';
                return $html;
            });
        $grid->column('home_show', '¿En home?')
            ->display(function ($activo) {
                return $activo ? 'Sí' : 'No';
            });
        


        $grid->perPages([10, 20, 30, 40, 50]);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Brand::findOrFail($id));

        $show->field('name', __('Nombre'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Brand);
        $slug = Str::slug($form->model()->name, '-');

        $form->switch('home_show', '')
            ->help('¿Mostrar en home?');
        $form->text('name', __('Nombre'))
            ->rules('required|unique:brands,name,{{id}}', [
                'required' => 'Este campo es requerido',
                'unique' => 'Ya existe registro con este nombre.',
            ]);
		$form->text('slug', __('Slug'))
            ->help('Se autogenera con él título');
        $form->image('home_image', __('Imagen'))
            ->removable()
            ->rules('required')
            ->help('Seleccione la imagen de la marca. Tamaño recomendado 223x58.');
            
        // $form->display('created_at', __('Created At'));
        // $form->display('updated_at', __('Updated At'));

        $form->saving(function (Form $form) {
            $form->slug = Str::slug($form->name, '-');
        });

        return $form;
    }
}
