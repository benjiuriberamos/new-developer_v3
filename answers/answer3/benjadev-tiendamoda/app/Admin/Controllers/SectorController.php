<?php

namespace App\Admin\Controllers;

use App\Models\Sector;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class SectorController extends AdminController
{
	/**
	 * Title for current resource.
	 *
	 * @var string
	 */
	protected $title = 'Sectores de ropa';

	/**
	 * Make a grid builder.
	 *
	 * @return Grid
	 */
	protected function grid()
	{
		$grid = new Grid(new Sector());

		$grid->column('id', __('ID'))->sortable();
		$grid->column('name', __('Nombre'));
		$grid->column('slug', __('Slug'));
		$grid->column('home_image', __('Imagen'))
			->display(function ($name) {
				$url =  $name ? Storage::url($name) : false;
				$html =  $url ? "<img src=' $url' width='100' heigth='20'></img>" : 'Sin imagen';
				return $html;
			});
        $grid->column('home_show', '¿En home?')
			->display(function ($activo) {
				return $activo ? 'Sí' : 'No';
			});


		return $grid;
	}

	/**
	 * Make a show builder.
	 *
	 * @param mixed $id
	 *
	 * @return Show
	 */
	protected function detail($id)
	{
		$show = new Show(Sector::findOrFail($id));

		$show->field('name', __('Nombre'));

		return $show;
	}

	/**
	 * Make a form builder.
	 *
	 * @return Form
	 */
	protected function form()
	{
		$form = new Form(new Sector());
		$slug = Str::slug($form->model()->name, '-');

		$form->switch('home_show', '')
            ->help('¿Mostrar en home?');
		$form->text('name', __('Nombre'))
			->rules('required|unique:sectors,name,{{id}}', [
				'required' => 'Este campo es requerido',
				'unique' => 'Ya existe registro con este nombre.',
			]);
		$form->text('slug', __('Slug'))->help('Se autogenera con él título');
		$form->image('home_image', __('Imagen'))
			->removable()
            ->help('Seleccione la imagen de la marca. Tamaño recomendado 844x380.');
		$form->text('home_text', __('Texto'))
			->rules('max:55', [
				'max' => 'Este campo es requerido',
			])
            ->help('Ingrese breve texto que describe este sector.');
		$form->text('home_btn_text', __('Texto del botón'))
			->rules('max:35', [
				'max' => 'Este campo es requerido',
			])
            ->help('Ingrese el texto del botón.');
		// $form->display('created_at', __('Created At'));
		// $form->display('updated_at', __('Updated At'));

		$form->saving(function (Form $form) {
			$form->slug = Str::slug($form->name, '-');
		});

		return $form;
	}
}
