<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use App\Models\PageSettings;
use Illuminate\Support\Str;
use App\Admin\Controllers\Subcore\SimplePageController;

class PageSettingsController extends SimplePageController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Settings';

    /**
     * Routes to redirect .
     *
     * @var array
     */
    protected $routes = [
        'edit' => 'admin.settings.edit',
        'update' => 'admin.settings.update',
        //param name for crud see with comand "php artisan route:list"
        'param' => 'setting',
    ];

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        $model = PageSettings::firstOrCreate(array());
        $form = new Form($model);

        $form->tab('Generales', function ($form) {
            $form->image('logo', __('Logo'))
                ->removable();
            $form->image('logomenu', __('Logo menu'))
                ->removable();
            $form->simpleeditor('descriplogo', __('Descripción'))
                ->simple();
            $form->text('email', __('Email'));
            $form->text('address', __('Dirección'));
            $form->list('phone_items', __('Teléfonos'));
            $form->text('currency', __('Moneda'));
        });

        $form->tab('Tarjetas de crédito', function ($form) {
            $form->text('credit_card_title', __('Titulo'))
                ->help('Este texto solo se ve en el detalle de un producto');
            $form->multipleImage('credit_card_items', 'Imagen de tajeta')
                ->removable();
        });

        $form->tab('Redes sociales', function ($form) {
            $form->text('url_facebook', __('Enlace Facebook'));
            $form->text('url_twiter', __('Enlace Twiter'));
            $form->text('url_instagram', __('Enlace Instagram'));
            $form->text('url_pinterest', __('Enlace Pinterest'));
            $form->text('url_youtube', __('Enlace Youtube'));
            $form->text('url_skype', __('Enlace Skype'));
        });

        $form->tab('Estrategia', function ($form) {
            $form->collection('delivery_items', '', function ($table) {
                $table->select('icono', 'Icono')
                    ->options(config('template.icon-delivery'))
                    ->rules('required');
                $table->text('title', 'Título')
                    ->rules('required');
                $table->text('subtitle', 'Subtítulo')
                    ->rules('required');
            });
        });

        $form->tab('Formulario whatsapp', function ($form) {
            $form->text('phone_whatsapp', __('Número de whatsapp'));
        });

        $form->setAction(route($this->routes['update'], [$this->routes['param'] => 1]));

        return $form;
    }

    
}
