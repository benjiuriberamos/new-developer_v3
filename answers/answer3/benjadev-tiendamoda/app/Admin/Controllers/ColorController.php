<?php

namespace App\Admin\Controllers;

use App\Models\Color;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Str;

class ColorController extends AdminController
{
	/**
	 * Title for current resource.
	 *
	 * @var string
	 */
	protected $title = 'Color de ropa';

	/**
	 * Make a grid builder.
	 *
	 * @return Grid
	 */
	protected function grid() 
    {
		$grid = new Grid(new Color());

		$grid->column('id', __('ID'))->sortable();
		$grid->column('name', __('Nombre'));
		$grid->column('slug', __('Slug'));
        $grid->column('code', __('Código'))->display(function ($title) {

            return $title . "<span style='background-color:$title; border-radius:100%;' width='10px'></span>";
        
        });
        // $grid->column('code', __('Código'))->display(function ($code) {
        //     return $code . `<span 
        //         style="background-color:'$code'; border-radius: 25%, width: 10px; border-color:#fff;">
        //         </span>`;
        // });

		$grid->perPages(array(10, 20, 30, 40, 50));

		return $grid;
	}

	/**
	 * Make a show builder.
	 *
	 * @param mixed $id
	 *
	 * @return Show
	 */
	protected function detail($id)
    {
		$show = new Show(Color::findOrFail($id));

		$show->field('name', __('Nombre'));
		$show->field('slug', __('Slug'));
		$show->field('code', __('Código'));

		return $show;
	}

	/**
	 * Make a form builder.
	 *
	 * @return Form
	 */
	protected function form()
    {
		$form = new Form(new Color());
		$slug = Str::slug($form->model()->name, '-');

		$form->text('name', __('Nombre'))
			->rules('required|unique:colors,name,{{id}}', [
				'required' => 'Este campo es requerido',
				'unique' => 'Ya existe registro con este nombre.',
			]);
		$form->text('slug', __('Slug'))->help('Se autogenera con él título');
        $form->color('code', __('Código'))->default('#fff')->help('Selecciona el color.');
		$form->display('created_at', __('Created At'));
		$form->display('updated_at', __('Updated At'));

		$form->saving(function (Form $form) {
			$form->slug = Str::slug($form->name, '-');
		});

		return $form;
	}
}
