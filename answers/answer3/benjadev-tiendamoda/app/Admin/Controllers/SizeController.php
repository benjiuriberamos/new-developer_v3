<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Size;
use Encore\Admin\Controllers\AdminController;
use Illuminate\Support\Str;

class SizeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Tallas de ropa';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Size);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', __('Nombre'));
        $grid->column('slug', __('Slug'));


        $grid->perPages([10, 20, 30, 40, 50]);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Size::findOrFail($id));

        $show->field('name', __('Nombre'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Size);
        $slug = Str::slug($form->model()->name, '-');

        $form->text('name', __('Nombre'))
            ->rules('required|unique:sizes,name,{{id}}', [
                'required' => 'Este campo es requerido',
                'unique' => 'Ya existe registro con este nombre.',
            ]);
		$form->text('slug', __('Slug'))->help('Se autogenera con él título');
        $form->display('created_at', __('Created At'));
        $form->display('updated_at', __('Updated At'));

        $form->saving(function (Form $form) {
            $form->slug = Str::slug($form->name, '-');
        });

        return $form;
    }
}
