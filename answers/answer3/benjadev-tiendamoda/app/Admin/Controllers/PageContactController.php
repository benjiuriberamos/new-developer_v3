<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use App\Models\PageContact;
use Illuminate\Support\Str;
use Encore\Admin\Form\Field\Table;
use App\Admin\Controllers\Subcore\SimplePageController;

class PageContactController extends SimplePageController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Página Contacto';

    /**
     * Routes to redirect .
     *
     * @var array
     */
    protected $routes = [
        'edit' => 'admin.contact.edit',
        'update' => 'admin.contact.update',
        //param name for crud see with comand "php artisan route:list"
        'param' => 'contact',
    ];

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $model = PageContact::firstOrCreate(array());
        $form = new Form($model);

        $form->tab('Títular', function ($form) {
            $form->switch('main_show', __('Bloque títular'))
                ->help('¿Mostrar bloque del títular?');
            $form->text('main_title', __('Títular'));
        });

        $form->tab('Mapa', function ($form) {
            $form->switch('map_show', __('Bloque mapa'))
                ->help('¿Mostrar bloque del mapa?');
            $form->textarea('map_iframe');
        });

        $form->tab('Formulario', function ($form) {
            $form->switch('form_show', __('Bloque formulario'))
                ->help('¿Mostrar bloque del formulario?');
        });

        $form->tab('Datos de contacto', function ($form) {
            $form->switch('contact_show', __('Bloque datos'))
                ->help('¿Mostrar bloque de datos?');
            $form->collection('contact_items', 'Datos de contacto', function ($table) {
                $table->select('icon', 'Tipo')->options([
                    'fa fa-envelope-open' => 'Mail',
                    'icon-call-header' => 'Teléfono',
                    'fa fa-map-marker' => 'Ubicación',
                ]);
                $table->simpleeditor('text1', 'Texto')
                    ->simple();
            });
        });

        $form->setAction(route($this->routes['update'], [$this->routes['param'] => 1]));

        return $form;
    }
}
