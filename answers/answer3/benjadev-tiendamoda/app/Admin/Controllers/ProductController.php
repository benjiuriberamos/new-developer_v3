<?php

namespace App\Admin\Controllers;

use App\Models\Size;
use App\Models\Brand;
use App\Models\Color;
use App\Models\Sector;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Encore\Admin\Controllers\AdminController;
use App\Admin\Controllers\Subcore\CompletePageController;

class ProductController extends CompletePageController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Productos de ropa';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('active', '¿¿Activo?')
            ->display(function ($activo) {
                return $activo ? 'Sí' : 'No';
            });
        $grid->column('home_show', '¿En home?')
            ->display(function ($activo) {
                return $activo ? 'Sí' : 'No';
            });
        $grid->column('title', __('Nombre'));
        $grid->column('image', __('Imagen'))->display(function ($name) {
                $url = Storage::url($name);
                return "<img src=' $url' width='20' heigth='20'></img>";
            });
        $grid->column('brand.name', __('Marca'));
        $grid->column('colors', __('Colores'))->display(function ($colors) {
            $html = '';
            foreach ($colors as $color) {
                $html .= "<span 
                class='badge badge-success' 
                style='background-color:{$color['code']}; color:#dee2e6;'
                >{$color['name']}</span>";
            }
            return $html;
        });
        $grid->column('sectors', 'Sectores')->display(function ($sectors) {
            $html = '';
            foreach ($sectors as $sector) {
                $html .= "<span class='badge badge-success'>{$sector['name']}</span>";
            }
            return $html;
        });
        $grid->column('categories', 'Categorias')->display(function ($categories) {
            $html = '';
            foreach ($categories as $category) {
                $html .= "<span class='badge badge-success'>{$category['name']}</span>";
            }
            return $html;
        });
        $grid->column('sizes', 'Tallas')->display(function ($sizes) {
            $html = '';
            foreach ($sizes as $size) {
                $html .= "<span class='badge badge-success'>{$size['name']}</span>";
            }
            return $html;
        });

        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        //Settings
        $grid->filter(function ($filter) {
            $filter->between('created_at', 'Created Time')->datetime();
        });
        $grid->perPages([10, 20, 30, 40, 50]);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));

        $show->field('id', __('ID'));
        $show->active('¿Activo?')->as(function ($content) {
            $content = $content ? 'Sí' : 'No';
            return $content;
        });
        $show->field('title', 'Nombre');
        $show->field('descripcion', 'Descripción');
        $show->image('image', 'Imagen');
        // $show->field('created_at', __('Created at'));
        // $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Product);

        $form->tab('General', function ($form) {
            $form->switch('active', __('¿Disponible?'))
                ->help('Marcar al producto como disponible. Si no esta como disponible no se mostrará en ninguna parte de la web.');
            $form->switch('home_show', '¿En home?')
                ->help('Marcar para mostrar en home?');
            $form->switch('is_top', '¿Es top?')
                ->help('Marcar como uno de los productos top.');
            $form->text('title', __('Nombre'));
            $form->text('slug', __('Slug'))->help('Se autogenera con el título.');
            $form->text('price', __('Precio'));
            $form->number('quantity', __('Cantidad'));
            $form->text('sku', __('Sku'));
            $form->text('material_text', __('Materiales'));
            $form->textarea('descripcion', __('Descripción'));
            $form->image('image', __('Imagen principal'))
                ->removable()
                ->rules('required')
                ->help('Seleccione las imagenes. Tamaño recomendado 740x900.');
            $form->multipleImage('image_items', __('Imagenes'))
                ->removable()
                ->help('Seleccione las imagenes. Tamaño recomendado 740x900.');
            $form->select('brand_id', __('Marca'))->options(Brand::all()->pluck('name', 'id'));
            $form->multipleSelect('colors', __('Colores'))->options(Color::all()->pluck('name', 'id'));
            $form->multipleSelect('sectors', __('Sectores'))->options(Sector::all()->pluck('name', 'id'));
            $form->multipleSelect('categories', __('Categories'))->options(Category::all()->pluck('name', 'id'));
            $form->multipleSelect('sizes', __('Tallas'))->options(Size::all()->pluck('name', 'id'));
        });

       

        $form->tab('Descripción', function ($form) {
            $form->switch('description_show', __('¿Mostrar bloque?'));
            $form->collection('description_items', '', function ($table) {
                $table->text('title', 'Título');
                $table->simpleeditor('text', 'Subtítulo')
                    ->full();
            });
        });

        $form->tab('Relacionados', function ($form) {
            $form->switch('related_show', __('¿Mostrar bloque?'));
            $form->text('related_title', __('Título'));
            $form->multipleSelect('products', __('Productos'))->options(Product::all()->pluck('title', 'id'));
        });

        $form->saving(function (Form $form) {
			$form->slug = Str::slug($form->title, '-');
		});

        return $form;
    }
}
