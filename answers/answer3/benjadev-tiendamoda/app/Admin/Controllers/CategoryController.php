<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use Encore\Admin\Controllers\AdminController;
use Illuminate\Support\Str;

class CategoryController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Categorias de ropa';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Category);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', __('Nombre'));
        $grid->column('slug', __('Slug'));
        $grid->column('home_image', __('Imagen'))
            ->display(function ($name) {
                $url =  $name ? Storage::url($name) : false;
                $html =  $url ? "<img src=' $url' width='20' heigth='20'></img>" : 'Sin imagen';
                return $html;
            });
        $grid->column('home_show', '¿En home?')
            ->display(function ($activo) {
                return $activo ? 'Sí' : 'No';
            });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Category::findOrFail($id));

        $show->field('name', __('Nombre'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Category());
        $slug = Str::slug($form->model()->name, '-');

        $form->switch('home_show', __('¿Mostrar en home?'));
        $form->switch('header_show', __('¿Mostrar en header?'));
        $form->switch('footer_show', __('¿Mostrar en footer?'));

        $form->text('name', __('Nombre'))
        ->rules('required|unique:categories,name,{{id}}', [
            'required' => 'Este campo es requerido',
            'unique' => 'Ya existe registro con este nombre.',
        ]);
        $form->text('slug', __('Slug'))
            ->help('Se autogenera con él título.');
        $form->image('home_image', __('Imagen'))
            ->removable()
            ->rules('required')
            ->help('Seleccione imagen de la categoría. Tamaño recomendado 1140x800');


        $form->saving(function (Form $form) {
            $form->slug = Str::slug($form->name, '-');
        });

        return $form;
    }
}
