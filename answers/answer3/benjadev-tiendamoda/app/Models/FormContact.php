<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageHome extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'form_contact';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
}
