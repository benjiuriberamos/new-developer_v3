<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBanerShowToHome extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home', function (Blueprint $table) {
            //$table->boolean('banner_show')->nullable();
            // $table->boolean('sector_show')->nullable();
            // $table->boolean('product_show')->nullable();

            $table->boolean('offert_show')->nullable();
            $table->string('offert_title', 85)->nullable();
            $table->string('offert_subtitle', 85)->nullable();
            $table->string('offert_image', 300)->nullable();
            $table->boolean('offert_btn_show')->nullable();
            $table->boolean('offert_btn_target')->nullable();
            $table->string('offert_btn_text', 35)->nullable();
            $table->string('offert_btn_url', 350)->nullable();

            $table->boolean('category_show')->nullable();
            $table->string('category_title', 85)->nullable();

            $table->boolean('brand_show')->nullable();

            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home', function (Blueprint $table) {
            $table->dropColumn('sector_show');
            $table->dropColumn('product_show');
            $table->dropColumn('offert_show');
            $table->dropColumn('offert_title');
            $table->dropColumn('offert_subtitle');
            $table->dropColumn('offert_image');
            $table->dropColumn('offert_btn_show');
            $table->dropColumn('offert_btn_target');
            $table->dropColumn('offert_btn_text');
            $table->dropColumn('offert_btn_url');
            $table->dropColumn('category_show');
            $table->dropColumn('category_title');
            $table->dropColumn('brand_show');

            //
        });
    }
}
