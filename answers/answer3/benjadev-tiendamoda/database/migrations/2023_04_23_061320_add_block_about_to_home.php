<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBlockAboutToHome extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home', function (Blueprint $table) {
            $table->boolean('clients_show')->nullable();
            $table->json('clients_items')->nullable();

            $table->boolean('about_show')->nullable();
            $table->string('about_title', 150)->nullable();
            $table->longText('about_description')->nullable();
            $table->string('about_image', 255)->nullable();

            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home', function (Blueprint $table) {
            $table->dropColumn('clients_show');
            $table->dropColumn('clients_items');

            $table->dropColumn('about_show');
            $table->dropColumn('about_title');
            $table->dropColumn('about_description');
            $table->dropColumn('about_image');
            //
        });
    }
}
