<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToAbout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('about', function (Blueprint $table) {
            $table->boolean('main_show')->nullable();
            $table->string('main_title', 150)->nullable();

            $table->boolean('history_show')->nullable();
            $table->string('history_title', 150)->nullable();
            $table->longText('history_description')->nullable();
            $table->string('history_mark', 255)->nullable();
            $table->string('history_image', 255)->nullable();
            $table->boolean('history_video_show')->nullable();
            $table->string('history_video_url')->nullable();

            $table->boolean('about_show')->nullable();
            $table->text('about_title', 150)->nullable();
            $table->json('about_items')->nullable();

            $table->boolean('team_show')->nullable();
            $table->string('team_title', 150)->nullable();
            //
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('about', function (Blueprint $table) {
            $table->dropColumn('main_show');
            $table->dropColumn('main_title');
            $table->dropColumn('history_show');
            $table->dropColumn('history_title');
            $table->dropColumn('history_description');
            $table->dropColumn('history_mark');
            $table->dropColumn('history_image');
            $table->dropColumn('history_video_show');
            $table->dropColumn('history_video_url');
            $table->dropColumn('about_show');
            $table->dropColumn('about_title');
            $table->dropColumn('about_items');
            $table->dropColumn('team_show');
            $table->dropColumn('team_title');
            //
        });
    }
}
