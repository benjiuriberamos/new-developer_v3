<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');

            //redes sociales
            $table->string('url_facebook', 300)->nullable();
            $table->string('url_twiter', 300)->nullable();
            $table->string('url_instagram', 300)->nullable();
            $table->string('url_pinterest', 300)->nullable();
            $table->string('url_youtube', 300)->nullable();
            $table->string('url_skype', 300)->nullable();

            //info
            $table->string('email', 300)->nullable();
            $table->string('address', 300)->nullable();
            $table->string('logo', 300)->nullable();
            $table->longText('descriplogo')->nullable();
            $table->json('phone_items')->nullable();

            $table->json('credit_card_items')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settigns');
    }
}
