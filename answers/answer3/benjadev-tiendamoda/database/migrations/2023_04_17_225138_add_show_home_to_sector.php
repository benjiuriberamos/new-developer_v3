<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShowHomeToSector extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sectors', function (Blueprint $table) {
            $table->boolean('home_show')->nullable();
            $table->string('home_text', 85)->nullable();
            $table->string('home_btn_text', 25)->nullable();
            $table->string('home_image', 300)->nullable();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sectors', function (Blueprint $table) {
            $table->dropColumn('home_show');
            $table->dropColumn('home_text');
            $table->dropColumn('home_btn_text');
            $table->dropColumn('home_image');
            //
        });
    }
}
