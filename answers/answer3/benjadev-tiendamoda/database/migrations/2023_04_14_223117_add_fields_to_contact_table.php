<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact', function (Blueprint $table) {
            //
            $table->boolean('main_show')->nullable();
            $table->string('main_title', 155)->nullable();

            $table->boolean('map_show')->nullable();
            $table->longText('map_iframe')->nullable();

            $table->boolean('form_show')->nullable();

            $table->boolean('contact_show')->nullable();
            $table->string('contact_title', 155)->nullable();
            $table->json('contact_items')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->dropColumn('main_show');
            $table->dropColumn('main_title');
            $table->dropColumn('map_show');
            $table->dropColumn('map_iframe');
            $table->dropColumn('form_show');
            $table->dropColumn('contact_title');
            $table->dropColumn('contact_items');
            //
        });
    }
}
