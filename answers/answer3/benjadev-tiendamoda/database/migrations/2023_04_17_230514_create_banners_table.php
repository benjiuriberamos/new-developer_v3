<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('imageb1', 300)->nullable();
            $table->string('imageb2', 300)->nullable();
            $table->string('imageb3', 300)->nullable();
            $table->string('title', 150)->nullable();
            $table->string('subtitle', 150)->nullable();
            $table->boolean('btn_show')->nullable();
            $table->boolean('btn_target')->nullable();
            $table->string('btn_text', 35)->nullable();
            $table->string('btn_url', 350)->nullable();
            
            $table->unsignedBigInteger('home_id');
            $table->foreign('home_id')->references('id')->on('home');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
