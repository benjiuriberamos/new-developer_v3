<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->boolean('active')->nullable();
            $table->string('name', 125)->nullable();
            $table->string('charge', 85)->nullable();
            $table->string('image', 300)->nullable();
            $table->string('facebook', 300)->nullable();
            $table->string('instagram', 300)->nullable();
            $table->string('twiter', 300)->nullable();
            
            $table->unsignedBigInteger('about_id')->nullable();
            $table->foreign('about_id')->references('id')->on('about');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
