<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFields2ToProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->text('material_text', 250)->nullable();
            $table->bigInteger('quantity')->nullable();
            $table->boolean('description_show')->nullable();
            $table->json('description_items')->nullable();
            $table->boolean('related_show')->nullable();
            $table->text('related_title', 250)->nullable();
            $table->text('sku', 250)->nullable();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('material_text');
            $table->dropColumn('quantity');
            $table->dropColumn('description_show');
            $table->dropColumn('description_items');
            $table->dropColumn('related_show');
            $table->dropColumn('related_title');
            $table->dropColumn('sku');
            //
        });
    }
}
