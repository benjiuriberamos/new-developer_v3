<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToHomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home', function (Blueprint $table) {
            //
            $table->boolean('banner_show')->nullable();
            $table->json('banner_items')->nullable();

            $table->boolean('sector_show')->nullable();

            $table->boolean('product_show')->nullable();

            $table->boolean('client_show')->nullable();
            $table->json('client_items')->nullable();

            $table->boolean('categories_show')->nullable();

            $table->string('main_title', 155)->nullable();

            $table->boolean('map_show')->nullable();
            $table->longText('map_iframe')->nullable();

            $table->boolean('form_show')->nullable();

            $table->boolean('contact_show')->nullable();
            $table->string('contact_title', 155)->nullable();
            $table->json('contact_items')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home', function (Blueprint $table) {
            //
            $table->dropColumn('banner_show');
            $table->dropColumn('banner_items');
            $table->dropColumn('sector_show');
            $table->dropColumn('product_show');
            $table->dropColumn('client_show');
            $table->dropColumn('client_items');
            $table->dropColumn('categories_show');
            $table->dropColumn('main_title');
            $table->dropColumn('map_show');
            $table->dropColumn('map_iframe');
            $table->dropColumn('form_show');
            $table->dropColumn('contact_show');
            $table->dropColumn('contact_title');
            $table->dropColumn('contact_items');
        });
    }
}
