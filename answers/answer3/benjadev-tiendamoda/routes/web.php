<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', '');
Route::get('/', 'Front\FrontController@home')->name('home');
Route::get('/nosotros', 'Front\FrontController@about')->name('about');
Route::get('/contacto', 'Front\FrontController@contact')->name('contact');
Route::get('/productos', 'Front\ProductController@products')->name('products');
Route::get('/producto/{slug}', 'Front\ProductController@product')
    ->name('product');
Route::get('/gracias-por-contactarnos/', 'Front\ThanksController@thanks')->name('thanks');

//Ajax
Route::get('/ajax/productos/', 'Front\AjaxController@products')->name('ajax.products');
Route::get('/ajax/productos/', 'Front\AjaxController@products')->name('ajax.products');


// Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
//     \UniSharp\LaravelFilemanager\Lfm::routes();
// });
// Route::group(['prefix' => 'laravel-filemanager'], function () {
//     \UniSharp\LaravelFilemanager\Lfm::routes();
// });
