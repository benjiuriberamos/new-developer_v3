@extends('errors::stoom')

@section('title', __('Unauthorized'))
@section('code', '401')
@section('message', __('Unauthorized'))
