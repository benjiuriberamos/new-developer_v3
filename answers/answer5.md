First, run this comands.

```
cd answer5
docker build -t benjadev/nodejs-image .
docker run --name app-answers -p 80:8080 -v $(PWD):/home/node/app -d benjadev/nodejs-image

```

After run comands you can see the answer in your browser, remember the port 80 must to be available.

```
http://localhost/
```
