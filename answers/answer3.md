This a project was build with php. It is a website with manageable blocks.

First, run this comands:

```
cd answer3/benjadev-tiendamoda
cp .env.example .env
nano .env
docker compose up --build -d
docker compose exec app rm -rf vendor composer.lock
docker compose exec app composer install
docker compose exec app php artisan key:generate
docker compose exec app php artisan migrate

```
